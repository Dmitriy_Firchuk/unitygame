﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelTime : MonoBehaviour {

	Text timeText;
	public static float time;
	public int timer=0;

	void Start () {
		timeText = GetComponent<Text> ();	
	}

	void Update () {
		time += Time.deltaTime;
		timer = (int)time;
		timeText.text = "Time: " + timer;
	}
}

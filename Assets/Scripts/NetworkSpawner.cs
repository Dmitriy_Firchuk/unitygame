﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkSpawner : NetworkBehaviour {

	public GameObject enemyPrefab;
	public PlayerHealthNetwork playerHealth;      

	[Command]
	void CmdSpawn () {

		//GameObject temp= (GameObject)Instantiate (enemyPrefab, transform.position, transform.rotation);
		//NetworkServer.Spawn (temp);
		//temp.Spawn
		//temp.Spawn (transform.position);
		//NetworkServer.Spawn (enemyPrefab);
		if(playerHealth.currentHealth <= 0f)
		{
			return;
		} 
		if (isServer) {
			GameObject temp = enemyPrefab.Spawn (transform.position);
			NetworkServer.Spawn (temp);
		}
	}
		

	void Start () {

		Invoke ("FindPlayerHealth", 2f);
		InvokeRepeating ("CmdSpawn", 3f, 3f);
	}

	void FindPlayerHealth() {

		var onlinePlayers = GameObject.FindObjectsOfType<OnlinePlayer>();

		foreach (var player in onlinePlayers)
		{
			if (player.isLocalPlayer)
			{
				playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealthNetwork> ();

				break;
			}
		}

		if (onlinePlayers.Length==0)
		{
			playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealthNetwork> ();
		}
	}
}

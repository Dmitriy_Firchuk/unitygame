﻿using UnityEngine;


public class EnemyManager : MonoBehaviour
{
	public PlayerHealth playerHealth;      
	public GameObject enemy;                
	public float spawnTime = 3f;            
	public Transform[] spawnPoints;         
	ObjectPoolScript objPool;
	int spawnPointIndex;

	void Start ()
	{		
		Invoke ("FindPlayerHealth", 2f);
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
	}
		
	void Spawn ()
	{
		if(playerHealth.currentHealth <= 0f)
		{
			return;
		} 
		spawnPointIndex = Random.Range (0, spawnPoints.Length);
		enemy.Spawn (spawnPoints [spawnPointIndex].position);
	}

	void FindPlayerHealth() {
		
		var onlinePlayers = GameObject.FindObjectsOfType<OnlinePlayer>();

		foreach (var player in onlinePlayers)
		{
			if (player.isLocalPlayer)
			{
				playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();

				break;
			}
		}

		if (onlinePlayers.Length==0)
		{
			playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();
		}
	}
}
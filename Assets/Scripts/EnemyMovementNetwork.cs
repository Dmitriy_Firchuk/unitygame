﻿using UnityEngine;
using UnityEngine.Networking;

public class EnemyMovementNetwork : NetworkBehaviour {

	Transform player;
	NavMeshAgent nav;
	EnemyHealthNetwork enemyHealth;
	PlayerHealthNetwork playerHealth;

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		enemyHealth = GetComponent<EnemyHealthNetwork> ();
		playerHealth = player.GetComponent<PlayerHealthNetwork> ();
		nav = GetComponent<NavMeshAgent> ();

	}

	void Start () {

	}

	void Update () {

		if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth>0) {
			nav.SetDestination (player.position);
		} 
		else
			nav.enabled = false;



	}
}
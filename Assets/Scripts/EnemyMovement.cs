﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	Transform player;
	NavMeshAgent nav;
	EnemyHealth enemyHealth;
	PlayerHealth playerHealth;

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		enemyHealth = GetComponent<EnemyHealth> ();
		playerHealth = player.GetComponent<PlayerHealth> ();
		nav = GetComponent<NavMeshAgent> ();

	}

	void Start () {
	
	}
	
	void Update () {

		if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth>0) {
			nav.SetDestination (player.position);
		} 
		else
			nav.enabled = false;


		
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerHealthNetwork : NetworkBehaviour 
	{
		public int startingHealth = 100; 
		[SyncVar]
		public int currentHealth;                                   
		//public Slider healthSlider; 
		public Slider healthSlider;
		public Image damageImage;                                   
		public AudioClip deathClip;                                 
		public float flashSpeed = 5f;                             
		public Color flashColour = new Color(1f, 0f, 0f, 0.1f);    

		Animator anim;                                              
		AudioSource playerAudio;                                   
		PlayerMovement playerMovement;                              
		PlayerShootingNetwork playerShooting;                              
		bool isDead;                                                
		bool damaged;                                               

		void Awake ()
		{
			anim = GetComponent <Animator> ();
			playerAudio = GetComponent <AudioSource> ();
			playerMovement = GetComponent <PlayerMovement> ();
		    playerShooting = GetComponentInChildren <PlayerShootingNetwork> ();
			currentHealth = startingHealth;
		}

		void Start() {
			Invoke ("FindPlayerSlider", 2f);
		}

		void Update ()
		{
			if (damaged)
			{
				damageImage.color = flashColour;
			}
			else
			{
				damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
			}
			damaged = false;
		}

		public void TakeDamage (int amount)
		{
			damaged = true;
			currentHealth -= amount;
			healthSlider.value = currentHealth;
			playerAudio.Play ();
			if(currentHealth <= 0 && !isDead)
			{
				Death ();
			}
		}

		void Death ()
		{
			isDead = true;
			playerShooting.CmdDisableEffects ();
			anim.SetTrigger ("Die");
			playerAudio.clip = deathClip;
			playerAudio.Play ();
			playerMovement.enabled = false;
			playerShooting.enabled = false;
		}       

		void FindPlayerSlider() {

			var onlinePlayers = GameObject.FindObjectsOfType<OnlinePlayer>();

			foreach (var player in onlinePlayers)
			{
				if (player.isLocalPlayer)
				{
					healthSlider = GameObject.FindGameObjectWithTag ("HealthSlider").GetComponent<Slider> ();
					damageImage = GameObject.FindGameObjectWithTag ("DamageImage").GetComponent<Image> (); 

					break;
				}
			}

			if (onlinePlayers.Length==0)
			{
				healthSlider = GameObject.FindGameObjectWithTag ("HealthSlider").GetComponent<Slider> ();
				damageImage = GameObject.FindGameObjectWithTag ("DamageImage").GetComponent<Image> (); 
			}
		}
	}

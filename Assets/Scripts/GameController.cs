﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	bool onPause = false;
	GameObject menuCanvas;

	void Start() {
		menuCanvas = GameObject.FindGameObjectWithTag("Menu");
		menuCanvas.SetActive (false);
		Time.timeScale = 1;
		LevelTime.time = 0;
	}

	public void PauseGame () {
		if (!onPause) {
			Time.timeScale = 0;
			onPause = true;
			menuCanvas.SetActive (true);

		} else {
			Time.timeScale = 1;
			onPause = false;
			menuCanvas.SetActive(false);
		}
	}

	public void RestartGame() {
		Application.LoadLevel (Application.loadedLevel);
	}

	public void MainMenu(string sceneName) {
		Application.LoadLevel(sceneName);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerShootingNetwork : NetworkBehaviour 
{
		public int damagePerShot = 20;   
		[SyncVar]
		public float timeBetweenBullets = 0.15f;       
		public float range = 100f;  
		[SyncVar]
		float timer;       
		Ray shootRay;                                   
		RaycastHit shootHit;                           
		int shootableMask;                             
		//ParticleSystem gunParticles;                  
		LineRenderer gunLine;                           
		AudioSource gunAudio;                          
		Light gunLight;       
		[SyncVar]
		float effectsDisplayTime = 0.2f;               

		void Awake ()
		{
			shootableMask = LayerMask.GetMask ("Shootable","Player");
			//	gunParticles = GetComponent<ParticleSystem> ();
			gunLine = GetComponentInChildren <LineRenderer> ();
			gunAudio = GetComponentInChildren<AudioSource> ();
			gunLight = GetComponentInChildren<Light> ();
		}

		void Update ()
		{
			timer += Time.deltaTime;

			if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets)
			{
				CmdShoot ();
			}

			if(timer >= timeBetweenBullets * effectsDisplayTime)
			{
				CmdDisableEffects ();
			}
		}
		[Command]
		public void CmdDisableEffects ()
		{
			gunLine.enabled = false;
			gunLight.enabled = false;
		}

		[Command]
		void CmdShoot ()
		{
			timer = 0f;
			gunAudio.Play ();
			gunLight.enabled = true;
			//gunParticles.Stop ();
			//gunParticles.Play ();
			gunLine.enabled = true;
			gunLine.SetPosition (0, gunLine.gameObject.transform.position);
			shootRay.origin = gunLine.gameObject.transform.position;
			shootRay.direction = gunLine.gameObject.transform.forward;

			if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
			{
			
			EnemyHealthNetwork enemyHealth = shootHit.collider.GetComponent <EnemyHealthNetwork> ();

				if(enemyHealth != null)
				{
					enemyHealth.RpcTakeDamage (damagePerShot, shootHit.point);
				}
				gunLine.SetPosition (1, shootHit.point);
			}

			else
			{
				gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
			}
		}
	}
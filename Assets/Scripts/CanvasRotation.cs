﻿using UnityEngine;
using System.Collections;

public class CanvasRotation : MonoBehaviour {

	RectTransform rect;
	GameObject mainCam;

	void Start () {
		rect = GetComponent<RectTransform> ();
		mainCam = GameObject.FindGameObjectWithTag ("MainCamera") as GameObject;
	}

	void Update () {
		rect.rotation = mainCam.transform.rotation;
	}
}

﻿/*using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CameraFollow : NetworkBehaviour
{
	public GameObject target;            
	public float smoothing = 5f;        
	Vector3 offset;                    

	void Start ()
	{
		
		Invoke ("FindPlayer", 1);
	}

	void FixedUpdate ()
	{ 
		Vector3 targetCamPos = target.transform.position + offset;
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
	}

	void FindPlayer () {

			target = GameObject.FindGameObjectWithTag ("Player");
			offset = transform.position - target.transform.position;
	}
} */
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	[SerializeField]
	float smoothing = 5f;

	Transform target;
	Vector3 offset;                    

	void Start()
	{
		Invoke("FindLocalPlayer", 1f);
	}

	void FixedUpdate()
	{ 
		Vector3 targetCamPos = target.position + offset;
		transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
	} 

	void FindLocalPlayer()
	{
		var onlinePlayers = GameObject.FindObjectsOfType<OnlinePlayer>();

		foreach (var player in onlinePlayers)
		{
			if (player.isLocalPlayer)
			{
				target = player.transform;
				offset = transform.position - target.position;

				break;
			}
		}

		if (onlinePlayers.Length==0)
		{
			GameObject temp=GameObject.FindGameObjectWithTag ("Player");
			target = temp.transform;
			offset = transform.position - target.transform.position;
			Debug.LogError("Can't find local player!");
		}
	}
}
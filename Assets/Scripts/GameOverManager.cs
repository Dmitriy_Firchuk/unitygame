﻿using UnityEngine;
using System.Collections;

public class GameOverManager : MonoBehaviour {

	public PlayerHealth playerHealth;
	Animator anim;
	public int winScore;
	bool isWin=false;

	void Start () {
		

		anim = GetComponent<Animator> ();
		if (isWin)
		Invoke ("WinGame", 3f);
		Invoke ("FindPlayerHealth", 2f);
	}

	void Update () {
	
		if (playerHealth.currentHealth <= 0)
			anim.SetTrigger ("GameOver");
		if (ScoreManager.score >= winScore) {
			anim.SetTrigger ("Win");
			isWin = true;
		}
	}

	void WinGame() {
			Time.timeScale = 0;
		}

	void FindPlayerHealth() {

		var onlinePlayers = GameObject.FindObjectsOfType<OnlinePlayer>();

		foreach (var player in onlinePlayers)
		{
			if (player.isLocalPlayer)
			{
				playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();

				break;
			}
		}

		if (onlinePlayers.Length==0)
		{
			playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();
		}
	}
}

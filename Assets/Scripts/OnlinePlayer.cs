﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class OnlinePlayer : NetworkBehaviour {


	void Start () {

		if (isLocalPlayer) {
			GetComponent<PlayerMovement> ().enabled = true;
			GetComponent<PlayerShootingNetwork> ().enabled = true;
		}
	}
}

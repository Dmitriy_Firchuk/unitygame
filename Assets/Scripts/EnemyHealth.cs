﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
	public int startingHealth = 100;            
	public int currentHealth;                   
	public float sinkSpeed = 2.5f;              
	public int scoreValue = 10;                 
	public AudioClip deathClip;   
	public AudioClip painClip;
	public GameObject enemyRagdoll;
	Animator anim;                              
	AudioSource enemyAudio;                     
	//ParticleSystem hitParticles;                
	CapsuleCollider capsuleCollider;            
	bool isDead;                                
	bool isSinking;  
	public Slider healthSlider;
	Rigidbody[] rig;
	Rigidbody rb;



	void Awake ()
	{
		Application.targetFrameRate = 200;
		anim = GetComponent <Animator> ();
		enemyAudio = GetComponent <AudioSource> ();
	//	hitParticles = GetComponentInChildren <ParticleSystem> ();
		capsuleCollider = GetComponent <CapsuleCollider> ();
		currentHealth = startingHealth;
		rig = GetComponentsInChildren<Rigidbody> ();
		rb = GetComponent <Rigidbody> ();
		rb.isKinematic = true;
	}

	void Update ()
	{ 
		if (isDead) {
			Invoke ("StartSinking", 3f);
		}
	}
		
	public void TakeDamage (int amount, Vector3 hitPoint)
	{
		
		if(isDead)
			return;	
		enemyAudio.Play ();
		currentHealth -= amount;
		healthSlider.value = currentHealth;
		//hitParticles.transform.position = hitPoint;
		//hitParticles.Play();
		if(currentHealth <= 0)
		{
			Death ();
		}
	}

	void Death ()
	{
		isDead = true;
		capsuleCollider.isTrigger = true;

		for (int i=0; i<rig.Length;i++){
			rig [i].isKinematic = false;
			rig [i].AddForce (-transform.forward * 200f);
		}
		//anim.SetTrigger ("Dead");
		GetComponent <NavMeshAgent> ().enabled = false;
		enemyAudio.clip = deathClip;
		enemyAudio.Play ();
		ScoreManager.score += scoreValue;
		anim.enabled = false;
	}
		
	public void StartSinking ()
	{
		isSinking = true;
		gameObject.Recycle ();
	}

	void OnEnable() {
		for (int i=0; i<rig.Length;i++){
			rig [i].isKinematic = true;
		}
	}

	void OnDisable() {
		CancelInvoke ();
		capsuleCollider.isTrigger = false;
		currentHealth = startingHealth;
		isDead = false;
		GetComponent <NavMeshAgent> ().enabled = true;
		anim.enabled = true;
		isSinking = false;
		healthSlider.value = currentHealth;
		enemyAudio.clip = painClip;
	}
}
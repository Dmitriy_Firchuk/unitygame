﻿using UnityEngine;
using System.Collections;

public class DestroyRagDoll : MonoBehaviour {

	Rigidbody[] rig;
	// Use this for initialization
	void Start () {
		rig = GetComponentsInChildren<Rigidbody> ();
		for (int i=0; i<rig.Length;i++){
			rig[i].AddForce (-transform.forward * 800f);
		}
		Destroy (gameObject, 3.5f);
		
	}

}
